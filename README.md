# miniki

A drop-in micro CMS running on PHP and Markdown.

## Installation

We'll need the **composer** package manager to install the necessary
dependencies, but we can quickly install it locally:

    cd ~
    mkdir -p ~/bin
    wget -O composer-setup.php https://getcomposer.org/installer
    php composer-setup.php --install-dir=$HOME/bin --filename=composer
    rm composer-setup.php

With composer installed, all dependencies can be set up with this command:

    composer install

Now, copy the `content/`, `assets/` and `lib/` directories, along with the
`index.php` file, to your desired destination dir.

(optional, Apache only) For clean URLs to work (`/hello` instead of
`/?page=hello`), rename the `htaccess` file to `.htaccess` and copy it
over as well.

Now open your project root in a browser and you should see the Miniki welcome
page.

## How to use

Just add files to the `content/` dir and you have a site structure. A file
called `hello.md` will be accessible at `https://example.com/hello`. The site
root will show the content of the `content/home.md` file.

## Requirements

The only required dependency is `php-xml`, which can usually be installed through
your system's package manager.

The other dependencies ([Parsedown](https://github.com/erusev/parsedown) and
[Spyc](https://github.com/mustangostang/spyc)) are included in the lib/
directory.

## Local development

In order to work on Miniki itself outside a server, it's handy to have a local environment. To run a local webserver to preview your Miniki site, just run

    php -S 127.0.0.1:8000

## License details

The license for Miniki is the AGPLv3, available in the LICENSE file.

The [Parsedown](https://github.com/erusev/parsedown) library is (c) Emanuil
Rusev and contributors, made available under the MIT license.

The [Spyc](https://github.com/mustangostang/spyc/) library is (c) Chris
Wanstrath, Vlad Andersen and contributors, made available under the MIT license.

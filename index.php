<!doctype html>
<?php
  // Uncomment these when developing
  error_reporting( E_ALL );
  ini_set( "display_errors", 1 );

  // Find dependencies
  // set_include_path(get_include_path() . ":lib/");

  // Set up CommonMark
  require_once('vendor/autoload.php');
  use League\CommonMark\Environment\Environment;
  use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
  use League\CommonMark\Extension\HeadingPermalink\HeadingPermalinkExtension;
  use League\CommonMark\Extension\TableOfContents\TableOfContentsExtension;
  use League\CommonMark\MarkdownConverter;

  $commonMarkConfig = [
      'table_of_contents' => [
          'html_class' => 'table-of-contents',
          'position' => 'top',
          'style' => 'bullet',
          'min_heading_level' => 1,
          'max_heading_level' => 6,
          'normalize' => 'relative',
          'placeholder' => '[TOC]',
      ],
  ];
  $environment = new Environment($commonMarkConfig);
  $environment->addExtension(new CommonMarkCoreExtension());
  $environment->addExtension(new HeadingPermalinkExtension());
  $environment->addExtension(new TableOfContentsExtension());
  $converter = new MarkdownConverter($environment);

  // Load config
  $configs = [];
  if(file_exists('config.php'))
    $configs = include('config.php');
  $plugins = [];
  if(array_key_exists('plugins', $configs))
    $plugins = $configs['plugins'];

  // Determine absolute URL for linking
  $base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off") ? "https" : "http");
  $base_url .= "://".$_SERVER['HTTP_HOST'];
  $base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

  // Get post name and content
  $pagename = isset($_GET['page']) ? $_GET['page'] : 'home';
  $content = file_get_contents('content/' . $pagename . '.md');

  // Process YAML frontmatter if present
  $metadata = [];
  if (strpos($content, "---\n") !== FALSE) {
    $parts = explode("\n---\n", $content, 2);
    $metadata = yaml_parse(substr($parts[0], 4));
    $content = $parts[1];
  }
  // Process markdown content
  $content = $converter->convert($content);
?>

<html>
  <head>
    <title><?php echo $metadata['title']; ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo $base_url ?>/assets/css/style.css">
    <link rel="icon" href="https://tilde.pt/favicon.png" type="image/x-icon">
    <?php
    foreach ($plugins as $plugin) {
      $plugindir = 'plugins/' . $plugin . '/';
      if(is_file($plugindir . 'head_include.php'))
        include($plugindir . 'head_include.php');
    }
    ?>
  </head>

  <body class="light">


    <div id="page-wrapper">
      <div class="column single-column">
        <header class="main-title">
          <h1>
            <?php echo $metadata['title']; ?>
          </h1>
        </header>
        <div class="content">
          <?php echo $content; ?>
        </div>
        <div id="demo"></div>

      </div><!-- /.column -->
    </div><!-- /#page-wrapper -->

    <?php
    foreach ($plugins as $plugin) {
      $plugindir = 'plugins/' . $plugin . '/';
      if(is_file($plugindir . 'body_include.php'))
        include($plugindir . 'body_include.php');
    }
    ?>
  </body>
</html>

  <?php
    if (array_key_exists('casts', $metadata)) {
      echo '<script src="' . $base_url . 'plugins/asciinema/assets/js/asciinema-player.min.js"></script>';
      echo '<script>';
      $casts = explode(",", $metadata['casts']);
      foreach ($casts as $cast) {
        $cast = trim($cast);
        echo "AsciinemaPlayer.create('cast/" . $cast . ".cast', document.getElementById('cast-" . $cast . "'), {";
        echo "  theme: 'solarized-light', loop: true, autoplay: true";
        echo "})";
      }
      echo '</script>';
    }
  ?>

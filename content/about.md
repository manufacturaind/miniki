---
title: About this site
---

# About this site

This site runs on Miniki, a micro-CMS powered by simple Markdown files.

[Go back home](home)
